# Índice de Snippets

**19 Snippets**  
  
## Snippets  
  
* **HTML *[7]***  

    1. **[html-template-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-template-main.code-snippets)**  

     ​	:question: *Crea una plantilla general de un fichero HTML.*  

    2. **[html-template-main-empty](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-template-main-empty.code-snippets)**  

     ​	:question: *Crea una plantilla general vacía de un fichero HTML.*  

    3. **[html-block](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-block.code-snippets)**  

     ​	:question: *Crea una separación de tipo bloque para un fichero HTML.*  

    4. **[html-wrapper-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-wrapper-main.code-snippets)**  

     ​	:question: *Crea un wrapper principal en un fichero HTML.*  

    5. **[html-box](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-box.code-snippets)**  

     ​	:question: *Crea una caja para contener elementos en HTML.*  

    6. **[html-metatags](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-metatags.code-snippets)**  

     ​	:question: *Crea todas las etiquetas meta de un ficcherto HTML.*  

    7. **[html-side](https://gitlab.com/angelhs/snippets-vsc/blob/master/html-side.code-snippets)**  

     ​	:question: *Crea un lado de un elemento en HTML.*  

---  
  
* **NodeJS *[3]***  

    1. **[node-template-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/node-template-main.code-snippets)**  

     ​	:question: *Crea una plantilla general de un fichero NodeJs.*  
  
    2. **[node-template-main-empty](https://gitlab.com/angelhs/snippets-vsc/blob/master/node-template-main-empty.code-snippets)**  

     ​	:question: *Crea una plantilla general vacía de un fichero NodeJs.*  
       
    3. **[node-block](https://gitlab.com/angelhs/snippets-vsc/blob/master/node-block.code-snippets)**  

     ​	:question: *Crea una separación de tipo bloque para un fichero NodeJs.*  
  
---  
  
* **Python *[3]***  

    1. **[py-template-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/py-template-main.code-snippets)**  

     ​	:question: *Crea una plantilla general de un fichero Python.*  
  
    2. **[py-template-main-empty](https://gitlab.com/angelhs/snippets-vsc/blob/master/py-template-main-empty.code-snippets)**  

     ​	:question: *Crea una plantilla general vacía de un fichero Python.*  
  
    2. **[py-block](https://gitlab.com/angelhs/snippets-vsc/blob/master/py-block.code-snippets)**  

     ​	:question: *Crea una separación de tipo bloque para un fichero Python.*  
  
---  
  
* **SCSS *[3]***  

    1. **[scss-template-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/scss-template-main.code-snippets)**  

     ​	:question: *Crea una plantilla general de un fichero SCSS.*  
  
    2. **[scss-template-main-empty](https://gitlab.com/angelhs/snippets-vsc/blob/master/scss-template-main-empty.code-snippets)**  

     ​	:question: *Crea una plantilla general vacía de un fichero SCSS.*  
  
    3. **[scss-block](https://gitlab.com/angelhs/snippets-vsc/blob/master/scss-block.code-snippets)**  

     ​	:question: *Crea una separación de tipo bloque para un fichero SCSS.*  
  
---  
  
* **CSS *[3]***  

    1. **[css-template-main](https://gitlab.com/angelhs/snippets-vsc/blob/master/css-template-main.code-snippets)**  

     ​	:question: *Crea una plantilla general de un fichero CSS.*  
  
    2. **[css-template-main-empty](https://gitlab.com/angelhs/snippets-vsc/blob/master/css-template-main-empty.code-snippets)**  

     ​	:question: *Crea una plantilla general vacía de un fichero CSS.*  
       
    3. **[css-block](https://gitlab.com/angelhs/snippets-vsc/blob/master/css-block.code-snippets)**  

     ​	:question: *Crea una separación de tipo bloque para un fichero CSS.*  