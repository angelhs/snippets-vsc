#  SNIPPETS EN VISUAL STUDIO CODE  

> **Autor**: *Ángel Herce Soto*  
> **Descripcion**: *Snippets para Visual Studio Code.*  
> **Repositorio**: *[https://gitlab.com/angelhs/snippets-vsc.git](https://gitlab.com/angelhs/snippets-vsc)*  
> **Índice**: *[https://gitlab.com/angelhs/snippets-vsc/blob/master/SNIPPETS.md](https://gitlab.com/angelhs/snippets-vsc/blob/master/SNIPPETS.md)*  

:exclamation: El proyecto tendrá que esta situado en ``%appdata%\Code\User\snippets`` y los snippets no podrán ir en subcarpetas para que funcionen en el ide.  




## 00 - Crear un Snippet  
``File -> Preferences -> User Snippets -> New Snippets.``  

Nos aparecerá algo parecido a el siguiente ejemplo, y nuestro código del **snippet** irá dentro de ``Body``.  

~~~javascript
{
    "Print to console": {
        "scope": "css, html, javascript",
        "prefix": "command",
        "body": [
            "Hello, ${1:this} is a ${2:snippet}.",
            "	${3}"
        ],
        "description": "This is a description of Snippet"
    }
}
~~~



## 01 - Marcadores

Las variables ``${1:this}``, ``${2:snippet}`` y ``${3}`` son opcionales, funcionan para posicionar el cursor y desplazarse de una variable a otra de forma ascendente tan solo con tabular, puedes añadir todas las variables que requieras, veamos lo un poco más a fondo:

1. ``${1}``: Por si solo indica que una vez el código se imprima el cursor se debe posicionar en esa posición.

2. ``:this``: Este es como un texto de ayuda para indicar cuál es el texto que debería de ir allí, así que es opcional.

:**NOTA:** *Si colocas el mismo numero de variable en dos o mas lugares distintos de tu código, le cursos se posicionará en todos a la vez.*  




## 02 - Prefix  

Funciona para añadir la abreviación o acceso directo del teclado a nuestro código, es decir, que con que escribamos nuestra palabra y tabulemos aparezca el contenido.  




## 03 - Scope  

Sirve para indicarle al ide en que tipo de archivos va a funcionar el snippet, si creamos un snippet para **HTML**, este no va a funcionar en archivos **JavaScript**.  




## 04 - Description  
Sirve para establecer una descripción del Snippet en cuestión.  




## 05 - Guardar el Snippet  

Recuerda guardarlo con la extensión **.code-snippets** y dentro de la carpeta ``%appdata%\Code\User\snippets``.




## 06 - Usar el Snippet  

Ya con nuestro snippet creado solo es cuestión de comenzar a usarlo, para eso existen 3 formas de hacerlo

* Añadiendo la palabra que agregaste en el **prefix** y luego pulsamos  la tecla ``tab`` (tabulador).
* ``Ctrl + Shift + P``, luego ``Insert Snippet`` y finalmente buscamos el snippet.  
* En ``File -> Preferences -> User Snippets.`` y luego buscamos el snippet.