# CHANGELOG  

## v1.0.0  
* **`NEW`** **`100-022`** *Se añade snippet que crea un lado de un elemento en HTML*  
* **`NEW`** **`100-021`** *Se añade snippet que crea todas las etiquetas meta de un ficcherto HTML*  
* **`NEW`** **`100-020`** *Se añade snippet que crea una caja para contener elementos en HTML*  
* **`NEW`** **`100-019`** *Se añade snippet que crea un wrapper principal en un fichero HTML*  
* **`NEW`** **`100-018`** *Se añade snippet que crea una separación de tipo bloque para un fichero HTML*  
* **`NEW`** **`100-017`** *Se añade snippet que crea una plantilla general vacía de un fichero HTML*  
* **`NEW`** **`100-016`** *Se añade snippet que crea una plantilla general de un fichero HTML*  
* **`NEW`** **`100-015`** *Se añade snippet que crea una separación de tipo bloque para un fichero NodeJs*  
* **`NEW`** **`100-014`** *Se añade snippet que crea una plantilla general vacía de un fichero NodeJs*  
* **`NEW`** **`100-013`** *Se añade snippet que crea una plantilla general de un fichero NodeJs*  
* **`NEW`** **`100-012`** *Se añade snippet que crea una separación de tipo bloque para un fichero Python*  
* **`FIX`** **`100-011`** *Se corrige el snippet `py-template-main-empty`*  
* **`FIX`** **`100-010`** *Se corrige el snippet `py-template-main`*  
* **`NEW`** **`100-009`** *Se añade snippet que crea una plantilla general vacía de un fichero Python*  
* **`NEW`** **`100-008`** *Se añade snippet que crea una plantilla general de un fichero Python*  
* **`NEW`** **`100-007`** *Se añade snippet que crea una separación de tipo bloque para un fichero SCSS*  
* **`NEW`** **`100-006`** *Se añade snippet que crea una plantilla general vacía de un fichero SCSS*  
* **`NEW`** **`100-005`** *Se añade snippet que crea una plantilla general de un fichero SCSS*  
* **`NEW`** **`100-004`** *Se añade snippet que crea una separación de tipo bloque para un fichero CSS*  
* **`NEW`** **`100-003`** *Se añade snippet que crea una plantilla general vacía de un fichero CSS*  
* **`NEW`** **`100-002`** *Se añade snippet que crea una plantilla general de un fichero CSS*  
* **`NEW`** **`100-001`** *Repositorio inicializado*  